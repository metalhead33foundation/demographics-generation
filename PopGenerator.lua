local demTables,json = dofile("PrepopData.lua")
local newTable = {}
local function outputTable(demTabula)
	local i = 1
	for i,row in pairs(demTabula) do
		row['popId'] = i
		row['areaID'] = row['area']['areaID']
		row['area'] = nil
		row['religionID'] = row['religion']['religionID']
		row['religion'] = nil
		row['raceId'] = row['race']['raceID']
		row['race'] = nil
		row['occupationID'] = row['occupation']['occupationID']
		row['occupation'] = nil
		row['agegroupID'] = row['agegroup']['agegrpID']
		row['agegroup'] = nil
		if row['female'] == "female" then
			row['female'] = true
		else
			row['female'] = false
		end
		i = i + 1
	end
	return json.stringify(demTabula)
end
function insertIntoTableArg(nRace, nReligion, nArea, nQuantity, nGender, nAgeGroups, nOccupations)
	local race = demTables:findRace(nRace)
	local religion = demTables:findReligion(nReligion)
	local area = demTables:findArea(nArea)
	local agegroup = demTables:findAgeGroup(nAgeGroups)
	--local occupation = demTables:findOccupation(nOccupations)
	for i,row in pairs(nOccupations) do
		local ntbl = {}
		ntbl.race = race
		ntbl.religion = religion
		ntbl.area = area
		ntbl.agegroup = agegroup
		ntbl.female = nGender
		ntbl.occupation = demTables:findOccupation(i)
		ntbl.quantity = math.ceil(nQuantity * row)
		table.insert(newTable,ntbl)
	end
end
function insertIntoTableTbl(nTable)
	local race = demTables:findRace(nTable.nRace)
	local religion = demTables:findReligion(nTable.nReligion)
	local area = demTables:findArea(nTable.nArea)
	local quantity = nTable.nQuantity
	--local occupation = demTables:findOccupation(nOccupations)
	for gender,genderTable in pairs(nTable.nGenders) do
		for agegroup,ageTable in pairs(genderTable['nGroups']) do
			local agePercentage = genderTable['nPercentage'] * ageTable['nPercentage']
			local allJobs = 0
			for occupationKey,occupationPercentage in pairs(ageTable['nJobs']) do
				allJobs = allJobs + occupationPercentage
			end
			for occupationKey,occupationPercentage in pairs(ageTable['nJobs']) do
				local ntbl = {}
				ntbl.race = race
				ntbl.religion = religion
				ntbl.area = area
				ntbl.agegroup = agegroup
				ntbl.female = gender
				ntbl.occupation = demTables:findOccupation(occupationKey)
				ntbl.quantity = math.ceil(quantity * (occupationPercentage / allJobs * agePercentage))
				table.insert(newTable,ntbl)
			end
		end
	end
end
local tempop = json.parse(json.readAll("NpopTemplate.json"))
for i,row in pairs(tempop['pops']) do
	insertIntoTableTbl(row)
end
local newestpops = io.open("NewestPops.json","w")
io.output(newestpops)
io.write(outputTable(newTable))
io.close(newestpops)
