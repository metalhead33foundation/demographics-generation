local json = dofile("json.lua")
function json.readAll(file)
    local f = io.open(file, "rb")
    local content = f:read("*all")
    f:close()
    return content
end
local countries = json.parse(readAll("Countries.json"))
local rawRegions = json.parse(readAll("Regions.json"))
local regions = {}
for i, row in ipairs(rawRegions['regions']) do
	row['country'] = countries['countries'][row['countryID']]
	row['countryID'] = nil
	table.insert(regions, row)
end
countries = nil
rawRegions = nil
local rawAreas = json.parse(readAll("Areas.json"))
local areas = {}
for i, row in ipairs(rawAreas['areas']) do
	row['region'] = regions[row['regionID']]
	row['regionID'] = nil
	table.insert(areas, row)
end
regions = nil
rawAreas = nil
local religions = json.parse(readAll("Religions.json"))
local races = json.parse(readAll("Races.json"))
local occupations = json.parse(readAll("Occupations.json"))
local agegroups = json.parse(readAll("AgeGroups.json"))
local rawPops = json.parse(readAll("Pops.json"))

local pops = {}
for i, row in ipairs(rawPops['pops']) do
	if row['quantity'] > 0 then
	row['area'] = areas[row['areaID']]
	row['areaID'] = nil
	row['religion'] = religions['religions'][row['religionID']]
	row['religionID'] = nil
	row['race'] = races['races'][row['raceId']]
	row['raceId'] = nil
	row['occupation'] = occupations['occupations'][row['occupationID']]
	row['occupationID'] = nil
	row['agegroup'] = agegroups['agegroups'][row['agegroupID']]
	row['agegroupID'] = nil
	table.insert(pops, row)
	end
end
local aras = nil
local religions = nil
local races = nil
local occupations = nil
local agegroups = nil
local rawPops = nil
return pops,json
