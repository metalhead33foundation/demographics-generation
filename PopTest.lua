local pops = dofile("Pops.lua")
for i,row in pairs(pops) do
	io.write("[" .. row.popId .. "] - ")
	io.write(row.race.raceName .. " - ")
	io.write(row.religion.religionName .. " - ")
	io.write(row.agegroup.agegrpName .. " - ")
	io.write(row.occupation.occupationName .. " - ")
	io.write(" [ " .. row.area.region.country.countryName .. " / " .. row.area.region.regionName .. " / " .. row.area.areaName .. " ]\n")
end
