local json = dofile("json.lua")
function json.readAll(file)
    local f = io.open(file, "rb")
    local content = f:read("*all")
    f:close()
    return content
end
local countries = json.parse(json.readAll("Countries.json"))
local rawRegions = json.parse(json.readAll("Regions.json"))
local regions = {}
for i, row in ipairs(rawRegions['regions']) do
	row['country'] = countries['countries'][row['countryID']]
	row['countryID'] = nil
	table.insert(regions, row)
end
countries = nil
rawRegions = nil
local rawAreas = json.parse(json.readAll("Areas.json"))
local demTables = {}
demTables.areas = {}
for i, row in ipairs(rawAreas['areas']) do
	row['region'] = regions[row['regionID']]
	row['regionID'] = nil
	table.insert(demTables.areas, row)
end
regions = nil
rawAreas = nil
demTables.religions = json.parse(json.readAll("Religions.json"))['religions']
demTables.races = json.parse(json.readAll("Races.json"))['races']
demTables.occupations = json.parse(json.readAll("Occupations.json"))['occupations']
demTables.agegroups = json.parse(json.readAll("AgeGroups.json"))['agegroups']
function demTables.findArea(self,areaname)
	for _,row in pairs(self.areas) do
		if row.areaName == areaname then
			print("Found the area " .. areaname .. "!")
			return row
		end
	end
	print("Didn't find the area " ..  areaname .. "!")
	return nil
end
function demTables.findReligion(self,relname)
	for _,row in pairs(self.religions) do
		if row.religionName == relname then
			print("Found the religion " .. relname .. "!")
			return row
		end
	end
	print("Didn't find the religion " ..  relname .. "!")
	return nil
end
function demTables.findRace(self,racename)
	for _,row in pairs(self.races) do
		if row.raceName == racename then
			print("Found the race " .. racename .. "!")
			return row
		end
	end
	print("Didn't find the race " ..  racename .. "!")
	return nil
end
function demTables.findOccupation(self,occname)
	for _,row in pairs(self.occupations) do
		if row.occupationName == occname then
			print("Found the occupation " .. occname .. "!")
			return row
		end
	end
	print("Didn't find the occupation " ..  occname .. "!")
	return nil
end
function demTables.findAgeGroup(self,agegroup_name)
	for _,row in pairs(self.agegroups) do
		if row.agegrpName == agegroup_name then
			print("Found the age group " .. agegroup_name .. "!")
			return row
		end
	end
	print("Didn't find the age group " ..  agegroup_name .. "!")
	return nil
end

return demTables,json
